package stackerr

import (
	"errors"
	"runtime/debug"
	"strings"
)

type StackErr struct {
	err   error
	stack []byte
}

func NewStackErr(err error) StackErr {
	return StackErr{
		err:   err,
		stack: debug.Stack(),
	}
}

func NewStackErrMsg(msg string) StackErr {
	return NewStackErr(errors.New(msg))
}

func (err StackErr) Error() string {
	return err.err.Error()
}

func (err StackErr) Unwrap() error {
	return err.err
}

func (err StackErr) Stack() []string {
	return strings.Split(string(err.stack), "\n")
}

func UnwrapStack(err error) []string {
	se, ok := err.(StackErr)
	if ok {
		return se.Stack()
	}

	child := se.Unwrap()
	if child == nil {
		return []string{}
	}

	return UnwrapStack(child)
}
